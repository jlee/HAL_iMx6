; Copyright 2017 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        [       :LNOT: :DEF: __HAL_VideoHDR__
        GBLL    __HAL_VideoHDR__

        GET     hdr.iMx6qIRQs

        GET     hdr:VideoDevice

 [ VideoInHAL
        GET     hdr:VIDCList

        GET     hdr:cpmem

; HDMI Transmitter encoder formats

; hdmi_datamap values
HDMI_RGB444_8B    * 0x01
HDMI_RGB444_10B   * 0x03
HDMI_RGB444_12B   * 0x05
HDMI_RGB444_16B   * 0x07
HDMI_YCbCr444_8B  * 0x09
HDMI_YCbCr444_10B * 0x0B
HDMI_YCbCr444_12B * 0x0D
HDMI_YCbCr444_16B * 0x0F
HDMI_YCbCr422_8B  * 0x16
HDMI_YCbCr422_10B * 0x14
HDMI_YCbCr422_12B * 0x12

; HDMI CSC encode format
; hdmi_csc_enc_format values
HDMI_eRGB      * 0x0
HDMI_eYCC444   * 0x1
HDMI_eYCC422   * 0x2
HDMI_eExtended * 0x3

; HDMI colorimetry defined by ITU
; hdmi_colorimetry values
HDMI_eITU601    * 0x0
HDMI_eITU709    * 0x1

; default IPU clock, derived from mmdc_ch0 clock
IPU_DEFAULT_WORK_CLOCK * 264000000

; DI counter allocation (they start from 0!)
InternalHSYNCCntr       *       1
OutputHSYNCCntr         *       2
OutputVSYNCCntr         *       3
ActiveLineCntr          *       4
ActivePixelCntr         *       5





; Hardware cursor size limits
HW_CURSOR_WIDTH         * 32
HW_CURSOR_HEIGHT        * 32
HW_CURSOR_WIDTH_POW2    * 5
 ]

VIDEO_IRQ               * IMX_INT_IPU1_FUNC
HDMI_IRQ                * IMX_INT_HDMI_TX

; -----------------------------------------------------------------------------------

 [ VideoInHAL
; VIDCList3 is terminated by a -1 word
; so min length of a vidc3 list...
VIDCList3_Size                  *       4+VIDCList3_ControlList-VIDCList3_Type

MaxPermittedPixelKHz            *       270000  ; max permitted pixel rate
 ]

; Device-specific struct for the VDU device

                        ^ 0
VDUDevSpec_SizeField    # 4 ; Size field
VDUDevSpec_Flags        # 4 ; Misc flags
VDUDevSpec_HDMI_TX_INT  # 4 ; hdmi transmitter interrupt number
VDUDevSpec_CCM_Base     # 4 ; CCM base address
VDUDevSpec_IOMUXC_Base  # 4 ; IOMUXC base address
VDUDevSpec_HDMI_Log     # 4 ; HDMI base address
VDUDevSpec_SRC_Log      # 4 ; System Reset unit logical address
VDUDevSpec_IPU1_Log     # 4 ;
VDUDevSpec_IPU2_Log     # 4 ;
VDUDevSpec_CCMAn_Log    # 4 ;
VDUDevSpec_Size         # 0 ; Size value to write to size field

                      ^    0, a1
; Public bits
VideoDeviceDevice     #    HALDevice_VDU_Size
; Private bits
VideoWorkspace        #    VDUDevSpec_Size
Video_DeviceSize      *    :INDEX: @

        ] ; __HAL_VideoHDR__

        END
